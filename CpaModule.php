<?php
namespace emilasp\cpa;

use yii\helpers\ArrayHelper;
use emilasp\core\CoreModule;

/**
 * Class CpaModule
 * @package emilasp\adv
 */
class CpaModule extends CoreModule
{
    public function behaviors()
    {
        return ArrayHelper::merge([

        ], parent::behaviors());
    }
}
