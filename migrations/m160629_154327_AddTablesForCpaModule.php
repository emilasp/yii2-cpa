<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m160629_154327_AddTablesForCpaModule extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->createTable('cpa_category', [
            'id'         => $this->primaryKey(11),
            'name'       => $this->string(255)->notNull(),
            'status'     => $this->smallInteger(1)->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_cpa_category_created_by',
            'cpa_category',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_cpa_category_updated_by',
            'cpa_category',
            'updated_by',
            'users_user',
            'id'
        );


        $this->createTable('cpa_platform', [
            'id'         => $this->primaryKey(11),
            'code'       => $this->string(50)->notNull(),
            'name'       => $this->string(255)->notNull(),
            'text'       => $this->text(),
            'link'       => $this->string(255)->notNull(),
            'data'       => $this->text(),
            'adverts'    => $this->text(),
            'geo'        => $this->text(),
            'status'     => $this->smallInteger(1)->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ], $this->tableOptions);


        $this->addForeignKey(
            'fk_cpa_platform_created_by',
            'cpa_platform',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_cpa_platform_updated_by',
            'cpa_platform',
            'updated_by',
            'users_user',
            'id'
        );

        $this->createTable('cpa_network', [
            'id'         => $this->primaryKey(11),
            'code'       => $this->string(50)->notNull(),
            'name'       => $this->string(255)->notNull(),
            'text'       => $this->text(),
            'link'       => $this->string(255)->notNull(),
            'data'       => $this->text(),
            'geo'        => $this->text(),
            'status'     => $this->smallInteger(1)->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ], $this->tableOptions);


        $this->addForeignKey(
            'fk_cpa_network_created_by',
            'cpa_network',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_cpa_network_updated_by',
            'cpa_network',
            'updated_by',
            'users_user',
            'id'
        );

        $this->createTable('cpa_offer', [
            'id'         => $this->primaryKey(11),
            'network_id' => $this->integer(),
            'code'       => $this->string(50)->notNull(),
            'name'       => $this->string(255)->notNull(),
            'text'       => $this->text(),
            'link'       => $this->string(255)->notNull(),
            'data'       => $this->text(),
            'geo'        => $this->text(),
            'status'     => $this->smallInteger(1)->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_cpa_offer_network_id',
            'cpa_offer',
            'network_id',
            'cpa_network',
            'id'
        );
        $this->addForeignKey(
            'fk_cpa_offer_created_by',
            'cpa_offer',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_cpa_offer_updated_by',
            'cpa_offer',
            'updated_by',
            'users_user',
            'id'
        );

        $this->createTable('cpa_place', [
            'id'         => $this->primaryKey(11),
            'code'       => $this->string(50)->notNull(),
            'name'       => $this->string(255)->notNull(),
            'text'       => $this->text(),
            'link'       => $this->string(255)->notNull(),
            'page'       => $this->string(255)->notNull(),
            'data'       => $this->text(),
            'geo'        => $this->text(),
            'status'     => $this->smallInteger(1)->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ], $this->tableOptions);


        $this->addForeignKey(
            'fk_cpa_place_created_by',
            'cpa_place',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_cpa_place_updated_by',
            'cpa_place',
            'updated_by',
            'users_user',
            'id'
        );

        $this->createTable('cpa_advert', [
            'id'          => $this->primaryKey(11),
            'platform_id' => $this->integer(),
            'place_id'    => $this->integer(),
            'network_id'  => $this->integer(),
            'type'        => $this->integer()->notNull(),
            'title'       => $this->string(100)->notNull(),
            'text'        => $this->string(255)->notNull(),
            'text2'       => $this->string(255)->notNull(),
            'image'       => $this->string(255)->notNull(),
            'data'        => $this->text(),
            'geo'         => $this->text(),
            'status'      => $this->smallInteger(1)->notNull(),
            'start'       => $this->dateTime(),
            'end'         => $this->dateTime(),
            'created_at'  => $this->dateTime(),
            'updated_at'  => $this->dateTime(),
            'created_by'  => $this->integer(11),
            'updated_by'  => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_cpa_advert_platform_id',
            'cpa_advert',
            'platform_id',
            'cpa_platform',
            'id'
        );
        $this->addForeignKey(
            'fk_cpa_advert_place_id',
            'cpa_advert',
            'place_id',
            'cpa_place',
            'id'
        );
        $this->addForeignKey(
            'fk_cpa_advert_network_id',
            'cpa_advert',
            'network_id',
            'cpa_network',
            'id'
        );
        $this->addForeignKey(
            'fk_cpa_advert_created_by',
            'cpa_advert',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_cpa_advert_updated_by',
            'cpa_advert',
            'updated_by',
            'users_user',
            'id'
        );

        //$this->createIndex('files_model', 'files_file', ['object', 'object_id', 'status']);

        $this->afterMigrate();
    }

    public function down()
    {
        //$this->dropTable('files_file');

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
